employees = [{'name':'Anne','salary':'45000','jobTitle':'developer'},
 {'name':'Anne','salary':45000,'jobTitle':'developer'},
 {'name':'Anne','salary':45000,'jobTitle':'developer'},
 {'name':'Anne','salary':45000,'jobTitle':'developer'}]


def is_developer(employee):
    return employee['jobTitle'] == 'developer'

developers = list(filter(is_developer,employees))

print(developers)

def get_salary(employee):
    return employee['salary']

developer_salaries = list(map(get_salary,developers))
print(developer_salaries)

is_developer = {f:i for i,f in enumerate(employees)}